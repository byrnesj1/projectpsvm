// terminal compilation (using gcc --version 5.4.0) needs following flags
// -std=c++11 -pthread
// (e.g. g++ -Wall filename.cpp -std=c++11 -pthread)

#include <iostream>
#include <thread>
#include <string>
using namespace std;



class Learner {
public:
	void operator()(int* values)
	{
		for (int inc = 0; inc <= (sizeof(values)/sizeof(*values)); inc++)
		{
			result += values[inc]; // placeholder for ML arithmetic
		}

		// typically want to file output here
		// then file read in main after collapsing threads (I think)
		// thread communication methodology still unclear

		cout << result;
	}
private:
	int result;
};

int main()
{
	// check how many threads are available in your local machine
	// std::thread::hardware_concurrency(); 

	int a[] = {1,2,3}; //theoretical subset of data
	int b[] = {3,4,5}; //theoretical subset of data
	
	thread t1((Learner()),a); //first thread
	thread t2((Learner()),b); //second thread
	
	// continuation of thread assignments dependant on # threads in local


	// following code is to measure timing of thread calculations relative to
	// timing of calculations in main
	// will be omitted in future versions
	
	for (int i = 0; i < 20; i++)
	{
		cout << "MAIN" << endl; 
	}


	// collapse threads
	t1.join();
	t2.join();



	
}