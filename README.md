#Sources for Project PSVM (Parallel Support Vector Machine)

Project could possibly include exploration into deep learning / quantum computing

##SVM & Machine Learning Theory

 - Hastie, Tibshirani, Friedman. "The Elements of Statistical Learning"
[link](http://statweb.stanford.edu/~tibs/ElemStatLearn/printings/ESLII_print10.pdf)

*Background theory for machine learning including SVM and NN Theory*




 - Andrew Ng. CS 229 (Stanford) Lecture Notes, Part V - Support Vector Machines.
[link](http://cs229.stanford.edu/materials.html)

*In depth theory regarding derivation of SVM algorithm.*



 - Sally Goldman. CS 582 (Washington Universtiy) Lecture Notes, Computational Learning Theory
[link](http://www.cs.wustl.edu/~sg/CS527_SP02/learning-theory-notes.pdf)

*Theory regarding the underlying theoretical problem of a programmable method of learning from data*




 - Boser, et al. "A Training Algorithm for Optimal Margin Classifiers"
[link](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.21.3818&rep=rep1&type=pdf) LIBSVM (Java, Python, MATLAB) including parallelization [link](https://github.com/arnaudsj/libsvm)

*The proposal of SVM as a ML solution*




 - http://domino.watson.ibm.com/library/cyberdig.nsf/papers/F2FAF0F788019701852574E1005095E3/$File/h0260.pdf



 - http://astro.temple.edu/~tua63862/AAAI2014Tutorial_final.pdf



## Parrallel and High Performance Computing

 - Dean, and Ghemawat. "MapReduce: Simplified Data Processing on Large Clusters"
[link](https://www.usenix.org/legacy/publications/library/proceedings/osdi04/tech/full_papers/dean/dean_html/)

*Outline of Google's implementation of MapReduce on clusters*




 - Victor Eijkhout. "Introduction to High Performance Scientific Computing"
[link](http://pages.tacc.utexas.edu/~eijkhout/Articles/EijkhoutIntroToHPC.pdf)

*Introduction to computational methods currently used in scientific settings to optimize efficiency, includes an introduction to parallel computing*




 - Akhter, Roberts. "Multi-Core Programming"
[link](http://users.nik.uni-obuda.hu/vamossy/SZPE2011/Segedletek/Multi-Core_Programming_Digital_Edition_(06-29-06).pdf)

*Full exposition on how to optimize efficiency using Intel Multi-Core processors*




 - Anthony Williams. "C++ Concurrency in Action"
[link](http://www.bogotobogo.com/cplusplus/files/CplusplusConcurrencyInAction_PracticalMultithreading.pdf)

*Reference on concurrecny (multithreading / parallel) computing in C++*




 - Bo Qian's C++ Threading Youtube Video Series [link](https://www.youtube.com/watch?v=LL8wkskDlbs)

*Quick basics on using C++11 to multithread programs*


 

 - The UPC Language [link](http://upc.lbl.gov/)

*The HUB of information regarding UPC Language. UPC is an approach at developing a language to seamlessly implement multithreading with shared memory* 



## Machine Learning Efficiency (parallel computing, proposed algs)

 - Chu, et al. "Map-Reduce for Machine Learning on Multicore"
[link](http://papers.nips.cc/paper/3150-map-reduce-for-machine-learning-on-multicore.pdf)

*Outlines how one could go about implementing parallelization of ML algorithms using multithreading, and the result the practitioner should expect*




 - Batiz-Benet, et al. "Parallelizing Machine Learning Algorithms"
[link](http://cs229.stanford.edu/proj2010/BatizBenetSlackSparksYahya-ParallelizingMachineLearningAlgorithms.pdf)

*Technical Report on developing and implementing parallelization of ML Algorithms*




 - Low, et al. "GraphLab: A New Framework For Parallel Machine Learning"
[link](http://www.select.cs.cmu.edu/publications/paperdir/uai2010-low-gonzalez-kyrola-bickson-guestrin-hellerstein.pdf)

*Introduces the GraphLab framework for parallelized ML*






 - Johnson, Zhang. "Accelerating Stochastic Gradient Descent using Predictive Variance Reduction" 
[link](http://papers.nips.cc/paper/4937-accelerating)

*Suggests a new algorithm to reduce convergence time for parameter estimation in ML*




 - Graf, et al. "Parallel Support Vector Machines: The Cascade SVM"
[link](http://papers.nips.cc/paper/2608-parallel-support-vector-machines-the-cascade-svm.pdf)

*Outlines a potential algorithm to parallelize SVMs*




 - Nguyen, et al. "Cascading Vector Machine" [link](https://stanford.edu/~rezab/classes/cme323/S15/projects/cascading_vector_machines_report.pdf). Source Code: [link](https://github.com/rikel/cvm)




 - Le, et al. "On Optimization Methods for Deep Learning"
[link](http://machinelearning.wustl.edu/mlpapers/paper_files/ICML2011Le_210.pdf)

*Discusses and illustrates weaknesses with Stochastic Gradient Descent optimization, and 
offers solutions*




## Quantum Computation

 - Leonard Susskin "Quantum Mechanics (The Theoretical Minimum)"

*Quantum Mechanics Theory*




 - Nielsen, Chuang "Quantum Computation and Quantum Information"

*Quantum Computation Theory*




 - Harrow, et al. "Quantum Algorithm for Linear Systems of Equations"
[link](https://arxiv.org/pdf/0811.3171.pdf)